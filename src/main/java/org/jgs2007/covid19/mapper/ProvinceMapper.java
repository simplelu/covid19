package org.jgs2007.covid19.mapper;

import org.apache.ibatis.annotations.Param;
import org.jgs2007.covid19.entity.ProvinceDto;
import org.springframework.stereotype.Repository;

/**
 * @author: luKai
 * @date： 2020/10/18 20:35
 * @description： 省份总体数据
 */
@Repository
public interface ProvinceMapper {

    int insert(@Param("provinceDTO") ProvinceDto provinceDTO);

}
