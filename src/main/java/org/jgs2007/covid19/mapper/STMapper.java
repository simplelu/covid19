package org.jgs2007.covid19.mapper;

import org.jgs2007.covid19.vo.SellerVo;
import org.jgs2007.covid19.vo.StockVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface STMapper {

    //全国已确诊人数
    List<SellerVo> getSeller();

    //全球感染前十国家感染人数和治愈人数
    List<StockVo> getStock();

}
