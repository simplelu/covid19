package org.jgs2007.covid19.mapper;

import org.apache.ibatis.annotations.Param;
import org.jgs2007.covid19.entity.WorldDto;
import org.springframework.stereotype.Repository;

/**
 * @author: luKai
 * @date： 2020/10/18 20:35
 * @description： 全球数据
 */
@Repository
public interface WorldMapper {

    int insert(@Param("worldDTO") WorldDto worldDTO);

}
