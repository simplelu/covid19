package org.jgs2007.covid19.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jgs2007.covid19.vo.IncrVo;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author: luKai
 * @date： 2020/10/18 18:35
 * @description： 世界地区实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorldDto {

    // 所属的洲
    private String continents;

    // 外国名称
    private String provinceName;

    // 现存确诊
    private Long currentConfirmedCount;

    // 累计确诊
    private Long confirmedCount;

    // 累计确诊排序
    private Long confirmedCountRank;

    // 疑似 ???
    private Long suspectedCount;

    // 治愈
    private Long curedCount;

    // 死亡
    private Long deadCount;

    // 死亡排序
    private Long deadCountRank;

    // 死亡率
    private String deadRate;

    // 死亡率排序
    private Long deadRateRank;

    private Long locationId;

    // 例：USA
    private String countryShortCode;

    // 例：United States of America
    private String countryFullName;

    // 该国家所有疫情数据下载地址（json格式）
    private String statisticsData;

    // 现存确诊、累计确诊、累计死亡、累计治愈 较昨日的变化
    private IncrVo incrVo;

    // 数据更新时间
    private Long createTime;

    // 数据更新时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;

    // 数据爬取时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date ctime;

}
