package org.jgs2007.covid19.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author: luKai
 * @date： 2020/10/18 15:37
 * @description： 省份或直辖市实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProvinceDto {

    // 省份或直辖市
    private String provinceName;

    // 简写地名
    private String provinceShortName;

    // 现存确诊
    private Long currentConfirmedCount;

    // 累计确诊
    private Long confirmedCount;

    // 疑似 ???
    private Long suspectedCount;

    // 治愈
    private Long curedCount;

    // 死亡
    private Long deadCount;

    // 对数据的额外描述
    private String comment;

    private Long locationId;

    // 该省份所有疫情数据下载地址（json格式）
    private String statisticsData;

    // 该省份下地区疫情详情
    private List<CityDto> cities;

    // 数据更新时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;

    // 数据爬取时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date ctime;
}
