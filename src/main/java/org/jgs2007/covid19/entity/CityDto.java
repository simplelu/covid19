package org.jgs2007.covid19.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author: luKai
 * @date： 2020/10/18 15:41
 * @description： 地区实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CityDto {

    // 地区
    private String cityName;

    // 现存确诊
    private Long currentConfirmedCount;

    // 累计确诊
    private Long confirmedCount;

    // 疑似 ???
    private Long suspectedCount;

    // 治愈
    private Long curedCount;

    // 死亡
    private Long deadCount;

    private Long locationId;

    // 数据更新时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;

    // 数据爬取时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date ctime;

}
