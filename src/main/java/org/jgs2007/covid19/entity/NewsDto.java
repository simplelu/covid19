package org.jgs2007.covid19.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: luKai
 * @date： 2020/10/18 15:03
 * @description： 新闻实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewsDto {

    // 新闻发布时间
    private Long pubDate;

    // 新闻标题
    private String title;

    // 新闻摘要
    private String summary;

    // 新闻来源（例：央视新闻app）
    private String infoSource;

    // 新闻链接
    private String sourceUrl;

}
