package org.jgs2007.covid19;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("org.jgs2007.covid19.mapper")
public class Covid19Application {

    public static void main(String[] args) {

        SpringApplication.run(Covid19Application.class, args);

        System.out.println("项目启动成功！");
    }

}
