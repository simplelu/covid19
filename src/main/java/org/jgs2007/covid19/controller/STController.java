package org.jgs2007.covid19.controller;

import org.jgs2007.covid19.service.STService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ming
 * @date 2020/11/2
 */
@RestController
@RequestMapping("stc")
public class STController {

    @Autowired
    private STService stService;

    @GetMapping("seller")
    public Object seller() {
        return stService.getSeller();
    }

    @GetMapping("stock")
    public Object stock() {
        return stService.getStock();
    }

}
