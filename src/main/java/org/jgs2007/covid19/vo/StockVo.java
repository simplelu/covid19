package org.jgs2007.covid19.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date： 2020/10/18 19:13
 * @description： 现感染前十国家现存感染人数和已经治愈感染人数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StockVo {

    // 国家名称
    private String name;

    // 现存感染人数
    private int stock;

    // 已经治愈感染人数
    private int sales;

}
