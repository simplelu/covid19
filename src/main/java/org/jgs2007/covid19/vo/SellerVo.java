package org.jgs2007.covid19.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date： 2020/10/18 19:13
 * @description： 省份名、累计确诊人数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SellerVo {

    // 省份名
    private String name;

    // 累计确诊人数
    private int value;

}
