package org.jgs2007.covid19.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: luKai
 * @date： 2020/10/18 19:13
 * @description： 现存确诊、累计确诊、累计死亡、累计治愈 较昨日的变化
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IncrVo {

    // 现存确诊较昨日变化
    private Long currentConfirmedIncr;

    // 累计确诊较昨日变化
    private Long confirmedIncr;

    // 累计治愈较昨日变化
    private Long curedIncr;

    // 累计死亡较昨日变化
    private Long deadIncr;
}
