package org.jgs2007.covid19.crawler;

import com.alibaba.fastjson.JSON;
import org.jgs2007.covid19.entity.NewsDto;
import org.jgs2007.covid19.entity.ProvinceDto;
import org.jgs2007.covid19.entity.WorldDto;
import org.jgs2007.covid19.mapper.CityMapper;
import org.jgs2007.covid19.mapper.ProvinceMapper;
import org.jgs2007.covid19.mapper.WorldMapper;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author: luKai
 * @date： 2020/10/18 13:44
 * @description： 爬取丁香医生疫情统计数据
 */
@Component
public class CovidCrawler {

    @Autowired
    private WorldMapper worldMapper;

    @Autowired
    private ProvinceMapper provinceMapper;

    @Autowired
    private CityMapper cityMapper;

    /**
     * 全球疫情数据
     * @return
     * @throws IOException
     */
    public void worldCrawler() throws IOException {

        String url = "https://ncov.dxy.cn/ncovh5/view/pneumonia?from=singlemessage&isappinstalled=0";

        Connection connection = Jsoup.connect(url);

        Document document = connection.get();

        String world = document.select("script[id=getListByCountryTypeService2true]").get(0).html();

        Matcher matcher = Pattern.compile("(?<= = ).+(?=}catch)").matcher(world);

        String data = null;

        if (matcher.find()) {

            data = matcher.group();

        }

        Date time = createTime();

        List<WorldDto> worldDTOS = JSON.parseArray(data, WorldDto.class);

        worldDTOS.forEach((w) -> {

            w.setTime(time);

            w.setCtime(new Date());

            worldMapper.insert(w);

        });

    }

    /**
     * 中国疫情数据
     * @return
     * @throws IOException
     */
    public void chinaCrawler() throws IOException {

        String url = "https://ncov.dxy.cn/ncovh5/view/pneumonia?from=singlemessage&isappinstalled=0";

        Connection connection = Jsoup.connect(url);

        Document document = connection.get();

        String china = document.select("script[id=getAreaStat]").get(0).html();

        Matcher matcher = Pattern.compile("(?<= = ).+(?=}catch)").matcher(china);

        String data = null;

        if (matcher.find()) {

            data = matcher.group();

        }

        Date time = createTime();

        List<ProvinceDto> provinceDTOS = JSON.parseArray(data, ProvinceDto.class);

        provinceDTOS.forEach((p) -> {

            p.setTime(time);

            p.setCtime(new Date());

            provinceMapper.insert(p);

            p.getCities().forEach((c) -> {

                c.setTime(time);

                c.setCtime(new Date());

                cityMapper.insert(p, c);

            });
        });

    }

    /**
     * 新闻实时播报（忽略）
     * @return
     * @throws IOException
     */
    public List<NewsDto> newsCrawler() throws IOException {

        String url = "https://file1.dxycdn.com/2020/0130/492/3393874921745912795-115.json?t=26717921";

        Connection connection = Jsoup.connect(url).ignoreContentType(true);

        String jsonString = connection.execute().body();

        String data = JSON.toJSONString(JSON.parseObject(jsonString, Map.class).get("data"));

        return JSON.parseArray(data, NewsDto.class);

    }

    /**
     * 获取疫情数据更新时间
     * @return
     */
    public Date createTime() throws IOException {

        String url = "https://ncov.dxy.cn/ncovh5/view/pneumonia?from=singlemessage&isappinstalled=0";

        Connection connection = Jsoup.connect(url);

        Document document = connection.get();

        String world = document.select("script[id=getListByCountryTypeService2true]").get(0).html();

        Matcher matcher = Pattern.compile("(?<= = ).+(?=}catch)").matcher(world);

        String data = null;

        if (matcher.find()) {

            data = matcher.group();

        }

        List<WorldDto> worldDTOS = JSON.parseArray(data, WorldDto.class);

        return new Date(worldDTOS.get(0).getCreateTime());

    }

}
