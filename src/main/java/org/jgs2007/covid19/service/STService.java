package org.jgs2007.covid19.service;

import org.jgs2007.covid19.vo.SellerVo;
import org.jgs2007.covid19.vo.StockVo;

import java.util.List;

/**
 *
 * @date 2020/11/2
 */
public interface STService {

    //全国已确诊人数
    List<SellerVo> getSeller();

    //全球感染前十国家感染人数和治愈人数
    List<StockVo> getStock();

}
