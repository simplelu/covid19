package org.jgs2007.covid19.service.impl;

import org.jgs2007.covid19.mapper.STMapper;
import org.jgs2007.covid19.service.STService;
import org.jgs2007.covid19.vo.SellerVo;
import org.jgs2007.covid19.vo.StockVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName: STServiceImpl
 * Description:
 * date: 2020/11/2 20:04
 *
 * @author 明
 * @since JDK 1.8
 */
@Service
public class STServiceImpl implements STService {

    @Autowired
    private STMapper stMapper;

    @Override
    public List<SellerVo> getSeller() {
        return stMapper.getSeller();
    }

    @Override
    public List<StockVo> getStock() {
        return stMapper.getStock();
    }
}
